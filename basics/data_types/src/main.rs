use std::collections::HashMap;

fn main() {
    scalar_types();
    string_type();
    tuple_type();
    array_type();
    enum_type();
    vector_collection();
    map_collection();
}

fn scalar_types(){
    println!(".....Scalar Types.....");
    let int_type:i32 = 2; // default
    let float_type:f64 = 2.2; // default
    let boolean_type:bool = false;
    let char_type:char = 'z';
}

fn string_type() {
    println!(".....String.....");
    let mut s = String::from("hello");
    s.push_str(", world!");
    println!("{}", s);

    println!(".....String Slice.....");
    let s = String::from("hello world");
    let hello = &s[0..5];
    let world = &s[6..11];
    println!("{}", hello);
    println!("{}", world);


}

fn tuple_type() {
    println!(".....Tuple.....");
    let tup:(u8,f64,i32) = (254, 2.2, 2515);
    let (x, y, z) = tup;
    println!("The value of tup_y is: {y}");
    let tup_x = tup.2;
    println!("The value of tup_x is: {tup_x}");
}

fn array_type() {
    println!(".....Array.....");
    let a = [1, 2, 3, 4, 5]; //fixed length
    let months = ["January", "February", "March", "April", "May", "June", "July",
        "August", "September", "October", "November", "December"];
    let jan = months[0];
    println!("{jan}");
    let feb = months[1];
    println!("{feb}");
}

fn vector_collection(){
    println!(".....Vector.....");
    // let mut vector1:Vec<i8> = Vec::new();
    let mut vector1= vec![2,4,6];
    vector1.push(2);
    vector1.push(3);
    vector1.push(4);

    println!(".....Option.....");
    let x = vector1.pop(); // x is 4
    if let Some(x) = x{
        println!("i: {}", x);
    }
}

fn map_collection() {
    println!(".....Map.....");
    //Map
    let mut h: HashMap<u8, bool> = HashMap::new();
    h.insert(5,true);
    h.insert(7,false);

    let have_five = h.get(&5).unwrap();
}


fn enum_type(){
    println!(".....Enum.....");
    //enum
    let color:Color = Color::Blue;
}

enum Color {
    Red,
    Yellow,
    Blue
}