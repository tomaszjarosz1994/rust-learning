#![allow(unused_variables)]

fn main() {
    simple_function();
    pass_reference_to_a_function();
    pass_mutable_reference_to_a_function();
}

fn simple_function() {
    println!(".....Simple function.....");
    let x = 3;
    let y = 4;

    let product = multiply(x, y);
    println!("Product of multiplication {} and {} is {}", x, y, product);
}

fn pass_reference_to_a_function() {
    println!(".....Pass reference to a function.....");
    let singular = String::from("apple");
    let plural = String::from("apples");
    is_plural(&singular);
    is_plural(&plural);
}

fn pass_mutable_reference_to_a_function() {
    println!(".....Pass mutable reference to a function.....");
    let mut singular = String::from("apple");
    let mut plural = String::from("apples");

    make_plural(&mut singular);
    make_plural(&mut plural);
    println!("Should be plural: {}", plural);
    println!("Also should be plural: {}", singular)
}

fn multiply(x: i32, y: i32) -> i32 {
    x * y
}

fn is_plural(s: &String) {
    println!("Check if plural");
    if s.ends_with("s") {
        println!("{} is plural", s);
    } else {
        println!("{} is singular", s);
    }
}

fn make_plural(s: &mut String) {
    println!("Make plural if singular");
    if !s.ends_with("s") {
        s.push_str("s")
    }
}