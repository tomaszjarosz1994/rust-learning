fn main() {
    let mut tasty_apple = Apple::new(true);
    let mut bitter_apple = Apple::new(false);

    let mut tasty_banana = Banana::new(true);
    let mut bitter_banana = Banana::new(false);

    for bite_count in 1..=5{
        println!();
        println!("Bite round: {}", bite_count);
        bite(&mut tasty_apple);
        bite(&mut tasty_banana);
        bite(&mut bitter_apple);
        bite(&mut bitter_banana);
        println!();
    }
}

fn bite<T:Bite + PrintInfo>(fruit: &mut T){
    fruit.info();
    fruit.bite();
    fruit.info();
}

trait Bite {
    fn bite(self: &mut Self);
}

trait PrintInfo {
    fn info(&self);
}

struct Apple {
    is_tasty: bool,
    how_much_left_percent: f32,
}

impl Apple {
    fn new(is_tasty: bool) -> Self {
        Self {
            is_tasty,
            how_much_left_percent: 100.0,
        }
    }

}

impl Bite for Apple {
    fn bite(self: &mut Self) {
        println!("Apple: taking a bite");
        self.how_much_left_percent = self.how_much_left_percent * 0.7
    }
}

impl PrintInfo for Apple {
    fn info(&self) {
        if self.is_tasty {
            println!("Im am tasty apple, {}% left", self.how_much_left_percent);
        } else {
            println!("Im am bitter apple, {}% left", self.how_much_left_percent);
        }
    }
}

struct Banana {
    is_tasty: bool,
    how_much_left_percent: f32,
}

impl Banana {
    fn new(is_tasty: bool) -> Self {
        Self {
            is_tasty,
            how_much_left_percent: 100.0,
        }
    }
}

impl Bite for Banana {
    fn bite(self: &mut Self) {
        println!("Banana: taking a bite");
        self.how_much_left_percent = self.how_much_left_percent * 0.8
    }
}

impl PrintInfo for Banana {
    fn info(&self) {
        if self.is_tasty {
            println!("Im am tasty banana, {}% left", self.how_much_left_percent);
        } else {
            println!("Im am bitter banana, {}% left", self.how_much_left_percent);
        }
    }
}