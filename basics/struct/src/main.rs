fn main() {
    simple_struct();
    tuple_struct();

    method_implementation();
}

fn simple_struct() {
    println!(".....Struct.....");
    let mut user1 = User {
        active: true,
        username: String::from("someusername123"),
        email: String::from("someone@example.com"),
        sign_in_count: 1,
    };

    user1.email = String::from("anotheremail@example.com");
    println!("{}", user1.email)
}

fn tuple_struct() {
    println!(".....Tuple Struct.....");
    let black = Color(0, 2, 3);
    let origin = Point(4, 5, 6);
    println!("{}", black.1);
    println!("{}", origin.2);
}

fn method_implementation() {
    println!(".....Method implementation.....");
    let rect1 = Rectangle {
        width: 30,
        height: 50,
    };

    println!(
        "The area of the rectangle is {} square pixels.",
        rect1.area()
    );
}

struct User {
    active: bool,
    username: String,
    email: String,
    sign_in_count: u64,
}

struct Color(i32, i32, i32);
struct Point(i32, i32, i32);

struct Rectangle {
    width: u32,
    height: u32,
}

impl Rectangle {
    fn area(&self) -> u32 {
        self.width * self.height
    }
}
