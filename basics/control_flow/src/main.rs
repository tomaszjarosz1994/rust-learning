fn main() {
    if_else_expresion();
    for_loop();
    while_loop();
    conditional_loop(8, "apple");
    iterate_through_array();
    iterator_loop();
}

fn if_else_expresion() {
    println!(".....if else.....");
    let number = 3;
    if number < 5 {
        println!("condition was true");
    } else {
        println!("condition was false");
    }
}

fn for_loop() {
    println!(".....for loop to sum up ints from 7 to 23 inclusive.....");
    let sum = sum(7, 23);
    println!("The sum is {}", sum);
}

fn while_loop() {
    println!(".....while loop to count how many times you can double x ( means x*2) until reach y.....");
    let times = double(1, 500);
    println!("You can double x {} times until x is larger than 500", times);
}

fn conditional_loop(times: i32, value: &str) {
    println!(".....Conditional loop to print value x times.....");
    let mut counter = 0;
    loop {
        counter += 1;
        if counter > times { break; }
        print!("{}x: {} ", counter, value);
        println!();
    };
}

fn iterate_through_array() {
    println!(".....Iterate through array.....");
    let a = [10, 20, 30, 40, 50];

    for element in a {
        println!("the value is: {element}");
    }
}

fn iterator_loop() {
    println!(".....iterator loop.....");
    for number in (1..4).rev() {
        println!("{number}!");
    }
    println!("LIFTOFF!!!");
}

fn sum(start: i32, end_inclusive: i32) -> i32 {
    let mut sum = 0;
    for i in start..=end_inclusive { //'dot dot equal' means inclusive
        sum += i;
    }
    sum
}

fn double(x: i32, y: i32) -> i32 {
    let mut count = 0;
    let mut x = x;

    while x < y {
        x *= 2;
        count += 1;
    }
    count
}
